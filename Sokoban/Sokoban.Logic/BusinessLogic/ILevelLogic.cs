﻿// <copyright file="ILevelLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Logic
{
    using System;
    using System.Collections.ObjectModel;
    using Sokoban.Model;

    /// <summary>
    /// ILevelLogic interface.
    /// </summary>
    public interface ILevelLogic
    {
        /// <summary>
        /// LoadLevels method.
        /// </summary>
        /// <param name="mapsName">Maps url.</param>
        /// <returns>List of level.</returns>
        ObservableCollection<Level> LoadLevels(string mapsName);

        /// <summary>
        /// LoadIsClearder method.
        /// </summary>
        /// <param name="lev">Levels.</param>
        /// <returns>List of level.</returns>
        ObservableCollection<Level> LoadIsClearder(ObservableCollection<Level> lev);

        /// <summary>
        /// Start sokoban.
        /// </summary>
        /// <param name="selectedLevel">Selected levels.</param>
        /// <param name="openSokobnFunc">openSokobnFunc.</param>
        void StartGame(Level selectedLevel, Func<Level, bool> openSokobnFunc);

        /// <summary>
        /// Clear level.
        /// </summary>
        /// <param name="levelId">Level id.</param>
        void LevelIsClearded(int levelId);
    }
}
