﻿// <copyright file="IUserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Logic
{
    using Sokoban.Model;

    /// <summary>
    /// IUserLogic interface.
    /// </summary>
    public interface IUserLogic
    {
        /// <summary>
        /// Get user from repo.
        /// </summary>
        /// <returns>User.</returns>
        User LoadUser();

        /// <summary>
        /// Save user.
        /// </summary>
        /// <param name="user">New user.</param>
        void SaveUser(User user);
    }
}
