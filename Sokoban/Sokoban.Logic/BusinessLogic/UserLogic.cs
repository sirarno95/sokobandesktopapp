﻿// <copyright file="UserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Logic
{
    using System;
    using System.IO;
    using System.Linq;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Sokoban.Model;

    /// <summary>
    /// UserLogic.
    /// </summary>
    public class UserLogic : IUserLogic
    {
        private readonly JObject setting;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserLogic"/> class.
        /// </summary>
        public UserLogic()
        {
            string json;

            // Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(this.settingsName);
            using (StreamReader reader = new StreamReader("Resources/Settings/settings.json"))
            {
                json = reader.ReadToEnd();
            }

            this.setting = JObject.Parse(json);
        }

        /// <summary>
        /// Event for refress data.
        /// </summary>
        public static event EventHandler RefestDate;

        /// <summary>
        /// Get user from repo.
        /// </summary>
        /// <returns>User.</returns>
        public User LoadUser()
        {
            string nick = (string)this.setting["settings"].FirstOrDefault(jt => (string)jt["type"] == "nick_name")["value"];
            return new User() { NickName = nick };
        }

        /// <summary>
        /// Save user.
        /// </summary>
        /// <param name="user">New user.</param>
        public void SaveUser(User user)
        {
            this.setting["settings"].FirstOrDefault(jt => (string)jt["type"] == "nick_name")["value"] = user.NickName;
            this.SaveToFile();
            RefestDate?.Invoke(this, EventArgs.Empty);
        }

        private void SaveToFile()
        {
            using (StreamWriter file = File.CreateText(@"Resources/Settings/settings.json"))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                this.setting.WriteTo(writer);
            }
        }
    }
}
