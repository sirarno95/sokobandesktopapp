﻿// <copyright file="SettingLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Logic
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Sokoban.Model;

    /// <summary>
    /// Class of SettingLogic.
    /// </summary>
    public class SettingLogic : ISettingLogic
    {
        private readonly JObject setting;

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingLogic"/> class.
        /// </summary>
        public SettingLogic()
        {
            string json;

            using (StreamReader reader = new StreamReader("Resources/Settings/settings.json"))
            {
                json = reader.ReadToEnd();
            }

            this.setting = JObject.Parse(json);
        }

        /// <summary>
        /// Get settings from repo.
        /// </summary>
        /// <returns>Settings.</returns>
        public ObservableCollection<Tile> LoadSetting()
        {
            // string nick = (string)this.setting["settings"].FirstOrDefault(jt => (string)jt["type"] == "nick_name")["value"];
            IEnumerable<Tile> settings = from x in this.setting["tiles"]
                                         select new Tile()
                                         {
                                             Type = (string)x["type"],
                                             TileId = (string)x["id"],
                                             Label = (string)x["label"],
                                             TileX = (int)x["type"],
                                             TileY = (int)x["type"],
                                         };

            return new ObservableCollection<Tile>(settings);
        }

        /// <summary>
        /// Get saved settings from repo.
        /// </summary>
        /// <returns>Saved settings.</returns>
        public ObservableCollection<Setting> LoadSavedSetting()
        {
            // string nick = (string)this.setting["settings"].FirstOrDefault(jt => (string)jt["type"] == "nick_name")["value"];
            IEnumerable<Setting> savedSettings = from x in this.setting["settings"]
                                                 select new Setting()
                                                 {
                                                     Type = (string)x["type"],
                                                     Value = (string)x["value"],
                                                 };

            return new ObservableCollection<Setting>(savedSettings);
        }

        /// <summary>
        /// Get floor settings from repo.
        /// </summary>
        /// <returns>Floor settings.</returns>
        public ObservableCollection<Tile> LoadFloorSetting()
        {
            IEnumerable<Tile> floorSettings = from x in this.setting["tiles"]
                                              where (string)x["type"] == "floor_brush"
                                              select new Tile()
                                              {
                                                  Type = (string)x["type"],
                                                  TileId = (string)x["id"],
                                                  Label = (string)x["label"],
                                                  TileX = (int)x["tileX"],
                                                  TileY = (int)x["tileY"],
                                              };

            return new ObservableCollection<Tile>(floorSettings);
        }

        /// <summary>
        /// Get floor goal settings from repo.
        /// </summary>
        /// <returns>Floor goal settings.</returns>
        public ObservableCollection<Tile> LoadFloorGoalSetting()
        {
            IEnumerable<Tile> floorGoalSettings = from x in this.setting["tiles"]
                                                  where (string)x["type"] == "floor_goal_brush"
                                                  select new Tile()
                                                  {
                                                      Type = (string)x["type"],
                                                      TileId = (string)x["id"],
                                                      Label = (string)x["label"],
                                                      TileX = (int)x["tileX"],
                                                      TileY = (int)x["tileY"],
                                                  };

            return new ObservableCollection<Tile>(floorGoalSettings);
        }

        /// <summary>
        /// Get wall settings from repo.
        /// </summary>
        /// <returns>Wall settings.</returns>
        public ObservableCollection<Tile> LoadWallSetting()
        {
            IEnumerable<Tile> wallSettings = from x in this.setting["tiles"]
                                             where (string)x["type"] == "wall_brush"
                                             select new Tile()
                                             {
                                                 Type = (string)x["type"],
                                                 TileId = (string)x["id"],
                                                 Label = (string)x["label"],
                                                 TileX = (int)x["tileX"],
                                                 TileY = (int)x["tileY"],
                                             };

            return new ObservableCollection<Tile>(wallSettings);
        }

        /// <summary>
        /// Get package settings from repo.
        /// </summary>
        /// <returns>Package settings.</returns>
        public ObservableCollection<Tile> LoadPackatSetting()
        {
            IEnumerable<Tile> packageSettings = from x in this.setting["tiles"]
                                                where (string)x["type"] == "packat_brush"
                                                select new Tile()
                                                {
                                                    Type = (string)x["type"],
                                                    TileId = (string)x["id"],
                                                    Label = (string)x["label"],
                                                    TileX = (int)x["tileX"],
                                                    TileY = (int)x["tileY"],
                                                };

            return new ObservableCollection<Tile>(packageSettings);
        }

        /// <summary>
        /// Get package goal settings from repo.
        /// </summary>
        /// <returns>Package goal settings.</returns>
        public ObservableCollection<Tile> LoadPackatGoalSetting()
        {
            IEnumerable<Tile> packageGoalSettings = from x in this.setting["tiles"]
                                                    where (string)x["type"] == "packat_goal_brush"
                                                    select new Tile()
                                                    {
                                                        Type = (string)x["type"],
                                                        TileId = (string)x["id"],
                                                        Label = (string)x["label"],
                                                        TileX = (int)x["tileX"],
                                                        TileY = (int)x["tileY"],
                                                    };

            return new ObservableCollection<Tile>(packageGoalSettings);
        }

        /// <summary>
        /// Get player settings from repo.
        /// </summary>
        /// <returns>Package goal settings.</returns>
        public ObservableCollection<Tile> LoadPlayerSetting()
        {
            IEnumerable<Tile> playerSettings = from x in this.setting["tiles"]
                                               where (string)x["type"] == "player_brush"
                                               select new Tile()
                                               {
                                                   Type = (string)x["type"],
                                                   TileId = (string)x["id"],
                                                   Label = (string)x["label"],
                                                   TileX = (int)x["tileX"],
                                                   TileY = (int)x["tileY"],
                                               };

            return new ObservableCollection<Tile>(playerSettings);
        }

        /// <summary>
        /// Save settings.
        /// </summary>
        /// <param name="settings">New settings.</param>
        public void SaveSettings(Tile[] settings)
        {
            foreach (var item in settings)
            {
                if (item.Type == "player_brush")
                {
                    this.setting["settings"].FirstOrDefault(jt => (string)jt["type"] == "is_highlighted")["value"] = item.TileId == "player_normal_brush" ? "true" : "false";
                }
                else
                {
                    this.setting["settings"].FirstOrDefault(jt => (string)jt["type"] == item.Type)["value"] = item.TileId;
                }
            }

            this.SaveToFile();
        }

        private void SaveToFile()
        {
            using (StreamWriter file = File.CreateText(@"Resources/Settings/settings.json"))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                this.setting.WriteTo(writer);
            }
        }
    }
}
