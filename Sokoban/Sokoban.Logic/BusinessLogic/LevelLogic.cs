﻿// <copyright file="LevelLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Xml.Linq;
    using Newtonsoft.Json.Linq;
    using Sokoban.Model;

    /// <summary>
    /// LevelLogic.
    /// </summary>
    public class LevelLogic : ILevelLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LevelLogic"/> class.
        /// </summary>
        public LevelLogic()
        {
            string json;

            using (StreamReader reader = new StreamReader("Resources/Settings/settings.json"))
            {
                json = reader.ReadToEnd();
            }

            this.Setting = JObject.Parse(json);
        }

        /// <summary>
        /// Gets or sets settings prop.
        /// </summary>
        public JObject Setting { get; set; }

        /// <summary>
        /// LoadLevels from xml.
        /// </summary>
        /// <param name="mapsName">XML url.</param>
        /// <returns>List of levels.</returns>
        public ObservableCollection<Level> LoadLevels(string mapsName)
        {
            int counter = 1;
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(mapsName);
            XDocument xdoc = XDocument.Load(stream);
            IEnumerable<Level> levelsIE = from x in xdoc.Descendants("Level")
                                          select new Level()
                                          {
                                              LevelId = counter++,
                                              Widht = int.Parse(x.Attribute("Width").Value),
                                              Height = int.Parse(x.Attribute("Height").Value),
                                              IsCleared = false,
                                          };

            List<Level> levels = levelsIE.ToList();

            // var a = from x in this.setting["cleared_levels"] select x["id"];
            (from x in this.Setting["cleared_levels"] select x["id"]).ToList().ForEach(id =>
                        {
                            levels.Where(level => level.LevelId == (int)id).ToList().ForEach(level => level.IsCleared = true);
                        });

            return new ObservableCollection<Level>(levels);
        }

        /// <summary>
        /// LoadIsClearder method.
        /// </summary>
        /// <param name="lev">Levels.</param>
        /// <returns>List of level.</returns>
        public ObservableCollection<Level> LoadIsClearder(ObservableCollection<Level> lev)
        {
            List<Level> levels = lev.ToList();

            // var a = from x in this.setting["cleared_levels"] select x["id"];
            (from x in this.Setting["cleared_levels"] select x["id"]).ToList().ForEach(id =>
            {
                levels.Where(level => level.LevelId == (int)id).ToList().ForEach(level => level.IsCleared = true);
            });

            return new ObservableCollection<Level>(levels);
        }

        /// <summary>
        /// Start sokoban.
        /// </summary>
        /// <param name="selectedLevel">Selected level.</param>
        /// <param name="openSokobnFunc">openSokobnFunc.</param>
        public void StartGame(Level selectedLevel, Func<Level, bool> openSokobnFunc)
        {
            openSokobnFunc?.Invoke(selectedLevel);
        }

        /// <summary>
        /// Clear level.
        /// </summary>
        /// <param name="levelId">Level id.</param>
        public void LevelIsClearded(int levelId)
        {
            JProperty mJProperty = new JProperty("id", levelId.ToString());
            JObject mJObject = new JObject(mJProperty);
            ((JArray)this.Setting["cleared_levels"]).Add(mJObject);
        }
    }
}
