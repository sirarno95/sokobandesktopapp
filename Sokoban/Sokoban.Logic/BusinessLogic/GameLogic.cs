﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using System.Xml.Linq;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Sokoban.Model;

    /// <summary>
    /// SocobanLogic.
    /// </summary>
    public class GameLogic
    {
        private readonly GameModel model;
        private readonly string mapsName;
        private readonly double stepsByCell = 8.0;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="model">Game model.</param>
        /// <param name="mapsName">Game map url.</param>
        /// <param name="selectedLevel">Selected level.</param>
        public GameLogic(GameModel model, string mapsName, int selectedLevel)
        {
            this.model = model;
            this.InitSettings();

            this.model.LevelId = selectedLevel == 0
                ? (int)this.model.Setting["settings"].FirstOrDefault(jt => (string)jt["type"] == "next_level")["value"]
                : selectedLevel;

            this.model.CurrentDirection = Direction.None;
            this.InitModel(mapsName);

            this.mapsName = mapsName;

            this.model.TheImage = new BitmapImage();
            this.model.TheImage.BeginInit();
            this.model.TheImage.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(@"Sokoban.Logic.Resources.Images.items.png");
            this.model.TheImage.EndInit();

            this.model.BackgroundImage = new BitmapImage();
            this.model.BackgroundImage.BeginInit();
            this.model.BackgroundImage.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(@"Sokoban.Logic.Resources.Images.sokoban_empty_bg.png");
            this.model.BackgroundImage.EndInit();

            this.model.ResetButtonImage = new BitmapImage();
            this.model.ResetButtonImage.BeginInit();
            this.model.ResetButtonImage.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(@"Sokoban.Logic.Resources.Images.restart_button.png");
            this.model.ResetButtonImage.EndInit();

            this.model.ExitButtonImage = new BitmapImage();
            this.model.ExitButtonImage.BeginInit();
            this.model.ExitButtonImage.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(@"Sokoban.Logic.Resources.Images.menu_button.png");
            this.model.ExitButtonImage.EndInit();

            this.model.MoveBackButtonImage = new BitmapImage();
            this.model.MoveBackButtonImage.BeginInit();
            this.model.MoveBackButtonImage.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(@"Sokoban.Logic.Resources.Images.back_button.png");
            this.model.MoveBackButtonImage.EndInit();

            this.model.Directions = new List<Direction>();
            this.model.PackagePushes = new List<bool>();
            this.model.SavedPackages = new List<bool[,]>();
            this.model.SavedPackagePoints = new List<List<Point>>();
        }

        /// <summary>
        /// Event for refress data.
        /// </summary>
        public static event EventHandler<int> RefestDateEvent;

        /// <summary>
        /// RefreshScreen event.
        /// </summary>
        public event EventHandler RefreshScreenEvent;

        /// <summary>
        /// Restart event.
        /// </summary>
        public event EventHandler RestartGameEvent;

        /// <summary>
        /// Exit event.
        /// </summary>
        public event EventHandler ExitGameEvent;

        /// <summary>
        /// Restart event.
        /// </summary>
        public event EventHandler MoveBackGameEvent;

        /// <summary>
        /// Mouse clicked.
        /// </summary>
        /// <param name="coordinates">Clicked position.</param>
        public void MouseClick(Point coordinates)
        {
            double y = (this.model.WindowHeight * 0.1 / 2) - 30;

            double xExit = this.model.WindowWidth - 100;
            double xReset = this.model.WindowWidth - 190;
            double xMoveBack = this.model.WindowWidth - 280;

            if (coordinates.X > xExit && coordinates.X < xExit + 60 && coordinates.Y > y && coordinates.Y < y + 60)
            {
                this.ExitGameEvent?.Invoke(this, EventArgs.Empty);
            }

            if (coordinates.X > xReset && coordinates.X < xReset + 60 && coordinates.Y > y && coordinates.Y < y + 60)
            {
                this.RestartGameEvent?.Invoke(this, EventArgs.Empty);
            }

            if (coordinates.X > xMoveBack && coordinates.X < xMoveBack + 60 && coordinates.Y > y && coordinates.Y < y + 60)
            {
                this.MoveBackGameEvent?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Start moving element.
        /// </summary>
        /// <param name="dx">Moving delta x.</param>
        /// <param name="dy">Moving delta y.</param>
        public void Move(double dx, double dy)
        {
            if (this.model.Player.X - Math.Truncate(this.model.Player.X) == 0 && this.model.Player.Y - Math.Truncate(this.model.Player.Y) == 0)
            {
                int newPlayerX = (int)(this.model.Player.X + dx);
                int newPlayerY = (int)(this.model.Player.Y + dy);

                if (newPlayerX >= 0 &&
                    newPlayerY >= 0 &&
                    newPlayerX < this.model.Walls.GetLength(0) &&
                    newPlayerY < this.model.Walls.GetLength(1) &&
                    !this.model.Walls[newPlayerX, newPlayerY])
                {
                    double movePlayerX = this.model.Player.X + (dx / this.stepsByCell);
                    double movePlayerY = this.model.Player.Y + (dy / this.stepsByCell);

                    if (this.model.Packages[newPlayerX, newPlayerY])
                    {
                        int newPackageX = (int)(this.model.Player.X + (2 * dx));
                        int newPackageY = (int)(this.model.Player.Y + (2 * dy));

                        if (!this.model.Walls[newPackageX, newPackageY] && !this.model.Packages[newPackageX, newPackageY])
                        {
                            this.model.PackagePushes.Add(true);
                            this.model.SavedPackages.Add(this.model.Packages.Clone() as bool[,]);
                            this.model.SavedPackagePoints.Add(new List<Point>(this.model.PackagePoints));

                            double movePackageX = newPlayerX + (dx / this.stepsByCell);
                            double movePackageY = newPlayerY + (dy / this.stepsByCell);

                            this.model.Packages[newPlayerX, newPlayerY] = false;
                            this.model.Packages[newPackageX, newPackageY] = true;

                            this.model.MovedPackagePoint = this.model.PackagePoints.Where(p => p.X == newPlayerX && p.Y == newPlayerY).First();
                            this.model.PackagePoints.RemoveAll(p => p.X == newPlayerX && p.Y == newPlayerY);

                            this.model.MovedPackagePoint = new Point(movePackageX, movePackageY);
                            this.model.Player = new Point(movePlayerX, movePlayerY);

                            if (dx == 0 && dy == -1)
                            {
                                this.model.CurrentDirection = Direction.Up;
                            }
                            else if (dx == 0 && dy == 1)
                            {
                                this.model.CurrentDirection = Direction.Down;
                            }
                            else if (dx == -1 && dy == 0)
                            {
                                this.model.CurrentDirection = Direction.Left;
                            }
                            else
                            {
                                this.model.CurrentDirection = Direction.Right;
                            }

                            this.model.Moves++;
                            this.model.Pushes++;

                            this.model.Directions.Add(this.model.CurrentDirection);
                        }
                    }
                    else
                    {
                        this.model.Player = new Point(movePlayerX, movePlayerY);

                        if (dx == 0 && dy == -1)
                        {
                            this.model.CurrentDirection = Direction.Up;
                        }
                        else if (dx == 0 && dy == 1)
                        {
                            this.model.CurrentDirection = Direction.Down;
                        }
                        else if (dx == -1 && dy == 0)
                        {
                            this.model.CurrentDirection = Direction.Left;
                        }
                        else
                        {
                            this.model.CurrentDirection = Direction.Right;
                        }

                        this.model.Moves++;
                        this.model.Directions.Add(this.model.CurrentDirection);
                        this.model.PackagePushes.Add(false);
                    }
                }
            }
        }

        /// <summary>
        /// Finish animated move.
        /// </summary>
        /// <returns>IsFinashed or no.</returns>
        public bool[] FinishMove()
        {
            double x = Math.Truncate(this.model.Player.X);
            double y = Math.Truncate(this.model.Player.Y);

            if (this.model.Player.X - x != 0 || this.model.Player.Y - y != 0)
            {
                switch (this.model.CurrentDirection)
                {
                    case Direction.Up:
                        this.model.Player = new Point(this.model.Player.X, this.model.Player.Y + (-1 / this.stepsByCell));
                        break;
                    case Direction.Down:
                        this.model.Player = new Point(this.model.Player.X, this.model.Player.Y + (1 / this.stepsByCell));

                        break;
                    case Direction.Left:
                        this.model.Player = new Point(this.model.Player.X + (-1 / this.stepsByCell), this.model.Player.Y);
                        break;
                    case Direction.Right:
                        this.model.Player = new Point(this.model.Player.X + (1 / this.stepsByCell), this.model.Player.Y);
                        break;
                    default:
                        break;
                }

                if (this.model.MovedPackagePoint.X != -1 && this.model.MovedPackagePoint.Y != -1)
                {
                    Point currentPackage = this.model.MovedPackagePoint;

                    switch (this.model.CurrentDirection)
                    {
                        case Direction.Up:
                            this.model.MovedPackagePoint = new Point(currentPackage.X, currentPackage.Y + (-1 / this.stepsByCell));
                            break;
                        case Direction.Down:
                            this.model.MovedPackagePoint = new Point(currentPackage.X, currentPackage.Y + (1 / this.stepsByCell));
                            break;
                        case Direction.Left:
                            this.model.MovedPackagePoint = new Point(currentPackage.X + (-1 / this.stepsByCell), currentPackage.Y);
                            break;
                        case Direction.Right:
                            this.model.MovedPackagePoint = new Point(currentPackage.X + (1 / this.stepsByCell), currentPackage.Y);
                            break;
                        default:
                            break;
                    }
                }

                if (this.model.Player.X - Math.Truncate(this.model.Player.X) == 0 && this.model.Player.Y - Math.Truncate(this.model.Player.Y) == 0)
                {
                    this.model.CurrentDirection = Direction.None;

                    if (this.model.MovedPackagePoint.X != -1 && this.model.MovedPackagePoint.Y != -1)
                    {
                        this.model.PackagePoints.Add(this.model.MovedPackagePoint);
                        this.model.MovedPackagePoint = new Point(-1, -1);
                    }
                }
            }

            var a = this.IsAllPackageIsCorrect(this.model.Packages, this.model.PackagesOnGoal);
            var b = this.model.Player.Equals(this.model.PlayerOnGoal);

            if (a && b)
            {
                this.LevelIsClearded();
            }

            return new bool[] { a, b };
        }

        /// <summary>
        /// Call refress update.
        /// </summary>
        public void TimerUpdate()
        {
            this.RefreshScreenEvent?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Restart the game, init map again.
        /// </summary>
        public void Restart()
        {
            this.InitModel(this.mapsName);
            this.model.Directions.Clear();
            this.model.PackagePushes.Clear();
            this.model.SavedPackages.Clear();
            this.model.SavedPackagePoints.Clear();
            this.model.Moves = 0;
            this.model.Pushes = 0;
            this.model.Helps = 0;
            this.model.GameTime.Restart();
        }

        /// <summary>
        /// Move back one step.
        /// </summary>
        public void MoveBack()
        {
            if (this.model.Player.X - Math.Truncate(this.model.Player.X) == 0 && this.model.Player.Y - Math.Truncate(this.model.Player.Y) == 0 && this.model.Directions.Any())
            {
                Direction player = this.RevertDirection(this.model.Directions.Last());

                switch (player)
                {
                    case Direction.Up:
                        this.model.Player = new Point(this.model.Player.X, this.model.Player.Y - 1);
                        break;
                    case Direction.Down:
                        this.model.Player = new Point(this.model.Player.X, this.model.Player.Y + 1);

                        break;
                    case Direction.Left:
                        this.model.Player = new Point(this.model.Player.X - 1, this.model.Player.Y);
                        break;
                    case Direction.Right:
                        this.model.Player = new Point(this.model.Player.X + 1, this.model.Player.Y);
                        break;
                    default:
                        break;
                }

                if (this.model.PackagePushes.Last())
                {
                    this.model.Packages = this.model.SavedPackages.Last();
                    this.model.PackagePoints = this.model.SavedPackagePoints.Last();

                    this.model.SavedPackages.RemoveAt(this.model.SavedPackages.Count - 1);
                    this.model.SavedPackagePoints.RemoveAt(this.model.SavedPackagePoints.Count - 1);

                    this.model.Pushes--;
                }

                this.model.Moves--;
                this.model.Helps++;

                this.model.Directions.RemoveAt(this.model.Directions.Count - 1);
                this.model.PackagePushes.RemoveAt(this.model.PackagePushes.Count - 1);

                this.model.CurrentDirection = Direction.None;
                this.RefreshScreenEvent?.Invoke(this, EventArgs.Empty);
            }
        }

        private void InitModel(string mapsName)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(mapsName);
            XDocument xdoc = XDocument.Load(stream);
            IEnumerable<XElement> levels = from x in xdoc.Descendants("Level") select x;
            XElement selectedLevel = levels.Skip(this.model.LevelId - 1).First();
            IEnumerable<XElement> firstLevelLines = from x in selectedLevel.Descendants("L") select x;
            string[] lines = firstLevelLines.Select(line => line.Value).ToArray();

            int width = (int)selectedLevel.Attribute("Width");
            int height = (int)selectedLevel.Attribute("Height");

            this.model.Floor = new bool[width, height];
            this.model.Walls = new bool[width, height];
            this.model.Packages = new bool[width, height];
            this.model.PackagesOnGoal = new bool[width, height];
            this.model.TileSize = Math.Min(
                (this.model.WindowWidth - (this.model.WindowWidth * 0.2)) / width,
                (this.model.WindowHeight - (this.model.WindowHeight * 0.2)) / height);

            this.model.GameWidth = width * this.model.TileSize;
            this.model.GameHeight = height * this.model.TileSize;

            this.model.PackagePoints = new List<Point>();
            this.model.Player = new Point(0, 0);

            for (int x = 0; x < height; x++)
            {
                bool isWall = false;
                int y = 0;
                foreach (char current in lines[x])
                {
                    if (current == '#')
                    {
                        isWall = true;
                    }

                    if ((isWall && x > 0 && x < height - 1) || current == '#')
                    {
                        this.model.Floor[y, x] = true;
                    }

                    this.model.Walls[y, x] = current == '#';
                    this.model.Packages[y, x] = current == '$';
                    this.model.PackagesOnGoal[y, x] = current == '.';

                    if (current == '$')
                    {
                        this.model.PackagePoints.Add(new Point(y, x));
                    }

                    if (current == '@')
                    {
                        this.model.Player = new Point(y, x);
                        this.model.PlayerOnGoal = new Point(y, x);
                    }

                    y++;
                }
            }

            for (int x = 1; x < this.model.Floor.GetLength(0) - 1; x++)
            {
                for (int y = 1; y < this.model.Floor.GetLength(1) - 1; y++)
                {
                    if (!this.model.Floor[x - 1, y] && !this.model.Walls[x, y])
                    {
                        this.model.Floor[x, y] = false;
                    }

                    if (!this.model.Floor[x - 1, y] && !this.model.Walls[x, y])
                    {
                        this.model.Floor[x, y] = false;
                    }

                    if (!this.model.Floor[x, y - 1] && !this.model.Walls[x, y])
                    {
                        this.model.Floor[x, y] = false;
                    }

                    if (!this.model.Floor[x, y + 1] && !this.model.Walls[x, y])
                    {
                        this.model.Floor[x, y] = false;
                    }
                }
            }

            for (int x = 1; x < this.model.Floor.GetLength(0) - 1; x++)
            {
                for (int y = this.model.Floor.GetLength(1) - 1; y < 1; y--)
                {
                    if (!this.model.Floor[x - 1, y] && !this.model.Walls[x, y])
                    {
                        this.model.Floor[x, y] = false;
                    }

                    if (!this.model.Floor[x - 1, y] && !this.model.Walls[x, y])
                    {
                        this.model.Floor[x, y] = false;
                    }

                    if (!this.model.Floor[x, y - 1] && !this.model.Walls[x, y])
                    {
                        this.model.Floor[x, y] = false;
                    }

                    if (!this.model.Floor[x, y + 1] && !this.model.Walls[x, y])
                    {
                        this.model.Floor[x, y] = false;
                    }
                }
            }

            if (this.model.Player == new Point(0, 0))
            {
                for (int i = 0; i < this.model.Floor.GetLength(0); i++)
                {
                    for (int j = 0; j < this.model.Floor.GetLength(1); j++)
                    {
                        if (this.model.Floor[i, j] && !this.model.Walls[i, j] && this.model.Player == new Point(0, 0) && !this.model.PackagesOnGoal[i, j]
                            && !this.model.Packages[i, j])
                        {
                            if (!(this.model.Walls[i - 1, j] || this.model.PackagesOnGoal[i - 1, j]) ||
                                !(this.model.Walls[i + 1, j] || this.model.PackagesOnGoal[i + 1, j]) ||
                                !(this.model.Walls[i, j - 1] || this.model.PackagesOnGoal[i, j - 1]) ||
                                !(this.model.Walls[i, j + 1] || this.model.PackagesOnGoal[i, j + 1]))
                            {
                                this.model.Player = new Point(i, j);
                                this.model.PlayerOnGoal = new Point(i, j);
                            }
                        }
                    }
                }
            }

            this.model.MovedPackagePoint = new Point(-1, -1);
        }

        private void InitSettings()
        {
            string json;
            using (StreamReader reader = new StreamReader("Resources/Settings/settings.json"))
            {
                json = reader.ReadToEnd();
            }

            this.model.Setting = JObject.Parse(json);
        }

        private bool IsAllPackageIsCorrect(bool[,] packages, bool[,] packagesOnGoal)
        {
            for (int x = 0; x < packages.GetLength(0); x++)
            {
                for (int y = 0; y < packages.GetLength(1); y++)
                {
                    if (packages[x, y] != packagesOnGoal[x, y])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void LevelIsClearded()
        {
            JProperty mJProperty = new JProperty("id", this.model.LevelId.ToString());
            JObject mJObject = new JObject(mJProperty);

            if (!(from x in this.model.Setting["cleared_levels"] select x["id"]).ToList().Contains(this.model.LevelId.ToString()))
            {
                ((JArray)this.model.Setting["cleared_levels"]).Add(mJObject);

                IOrderedEnumerable<JToken> sort = from x in this.model.Setting["cleared_levels"]
                                                  orderby x["id"]
                                                  select x;
                JArray mJArray = new JArray(sort);
                ((JArray)this.model.Setting["cleared_levels"]).Clear();
                this.model.Setting["cleared_levels"] = mJArray;

                // NEXT LEVEL!!!!!
                int next_level = 0;
                for (int i = 0; i < mJArray.Count(); i++)
                {
                    if ((i + 1) != (int)mJArray[i]["id"])
                    {
                        next_level = i + 1;
                    }
                }

                if (next_level == 0)
                {
                    next_level = mJArray.Count() + 1;
                }

                this.model.Setting["settings"].FirstOrDefault(jt => (string)jt["type"] == "next_level")["value"] = next_level.ToString();

                this.SaveToFile();
                RefestDateEvent?.Invoke(this, this.model.LevelId);
            }
        }

        private void SaveToFile()
        {
            using (StreamWriter file = File.CreateText(@"Resources/Settings/settings.json"))
            using (JsonTextWriter writer = new JsonTextWriter(file))
            {
                this.model.Setting.WriteTo(writer);
            }
        }

        private Direction RevertDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return Direction.Down;
                case Direction.Down:
                    return Direction.Up;
                case Direction.Left:
                    return Direction.Right;
                case Direction.Right:
                    return Direction.Left;
                case Direction.None:
                    return Direction.None;
                default:
                    return Direction.None;
            }
        }
    }
}
