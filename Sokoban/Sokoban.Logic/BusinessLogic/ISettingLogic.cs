﻿// <copyright file="ISettingLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Logic
{
    using System.Collections.ObjectModel;
    using Sokoban.Model;

    /// <summary>
    /// ISettings interface.
    /// </summary>
    public interface ISettingLogic
    {
        /// <summary>
        /// Load floor setting method.
        /// </summary>
        /// <returns>List of floor settings.</returns>
        ObservableCollection<Tile> LoadFloorSetting();

        /// <summary>
        /// Load floor gaol setting method.
        /// </summary>
        /// <returns>List of floor gaol settings.</returns>
        ObservableCollection<Tile> LoadFloorGoalSetting();

        /// <summary>
        /// Load wall setting method.
        /// </summary>
        /// <returns>List of wall settings.</returns>
        ObservableCollection<Tile> LoadWallSetting();

        /// <summary>
        /// Load packat setting method.
        /// </summary>
        /// <returns>List of packat settings.</returns>
        ObservableCollection<Tile> LoadPackatSetting();

        /// <summary>
        /// Load packat goal setting method.
        /// </summary>
        /// <returns>List of packat goal settings.</returns>
        ObservableCollection<Tile> LoadPackatGoalSetting();

        /// <summary>
        /// Load player setting method.
        /// </summary>
        /// <returns>List of player settings.</returns>
        ObservableCollection<Tile> LoadPlayerSetting();

        /// <summary>
        /// Load saved Setting method.
        /// </summary>
        /// <returns>List of settings.</returns>
        ObservableCollection<Setting> LoadSavedSetting();

        /// <summary>
        /// Save settings.
        /// </summary>
        /// <param name="settings">New settings.</param>
        void SaveSettings(Tile[] settings);
    }
}
