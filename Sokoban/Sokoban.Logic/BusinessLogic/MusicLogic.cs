﻿// <copyright file="MusicLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Logic
{
    using System;
    using System.IO;
    using System.Windows.Media;
    using Sokoban.Model;

    /// <summary>
    /// Class of MusicLogic.
    /// </summary>
    public class MusicLogic : IMusicLogic
    {
        private readonly Music music;
        private readonly MediaPlayer mediaPlayer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MusicLogic"/> class.
        /// </summary>
        public MusicLogic()
        {
            this.music = new Music() { IsPlay = false, MyImage = "../Resources/Images/sound_stop.png" };
            this.mediaPlayer = new MediaPlayer();
            this.mediaPlayer.Open(new Uri(Directory.GetCurrentDirectory() + @"\Resources\Music\Sokoban.mp3"));
        }

        /// <summary>
        /// Load musci.
        /// </summary>
        /// <returns>Music obj.</returns>
        public Music LoadMusic()
        {
            return this.music;
        }

        /// <summary>
        /// Contorl game music.
        /// </summary>
        public void MusicControl()
        {
            if (this.music.IsPlay)
            {
                this.music.IsPlay = false;
                this.music.MyImage = "../Resources/Images/sound_stop.png";
                this.mediaPlayer.Pause();

                // this.mediaPlayer.Stop();
            }
            else
            {
                this.music.IsPlay = true;
                this.music.MyImage = "../Resources/Images/sound_play.png";
                this.mediaPlayer.Play();
            }
        }
    }
}
