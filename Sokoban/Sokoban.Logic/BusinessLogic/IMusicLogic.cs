﻿// <copyright file="IMusicLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Logic
{
    using Sokoban.Model;

    /// <summary>
    /// Interface of MusicLogic.
    /// </summary>
    public interface IMusicLogic
    {
        /// <summary>
        /// Contorl game music.
        /// </summary>
        void MusicControl();

        /// <summary>
        /// Load musci.
        /// </summary>
        /// <returns>Music obj.</returns>
        Music LoadMusic();
    }
}
