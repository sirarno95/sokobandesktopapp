var hierarchy =
[
    [ "Application", null, [
      [ "Sokoban.App", "class_sokoban_1_1_app.html", null ],
      [ "Sokoban.App", "class_sokoban_1_1_app.html", null ],
      [ "Sokoban.App", "class_sokoban_1_1_app.html", null ],
      [ "Sokoban.App", "class_sokoban_1_1_app.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "Sokoban.WpfApp.Properties.Settings", "class_sokoban_1_1_wpf_app_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "Sokoban.WpfApp.Controls.SokobanControl", "class_sokoban_1_1_wpf_app_1_1_controls_1_1_sokoban_control.html", null ]
    ] ],
    [ "Sokoban.Logic.GameLogic", "class_sokoban_1_1_logic_1_1_game_logic.html", null ],
    [ "Sokoban.Model.GameModel", "class_sokoban_1_1_model_1_1_game_model.html", null ],
    [ "Sokoban.Renderer.GameRenderer", "class_sokoban_1_1_renderer_1_1_game_renderer.html", null ],
    [ "IComponentConnector", null, [
      [ "Sokoban.WpfApp.UserControls.UserControlDashboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_dashboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlDashboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_dashboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlDashboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_dashboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLeaderboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_leaderboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLeaderboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_leaderboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLeaderboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_leaderboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLevels", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_levels.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLevels", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_levels.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLevels", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_levels.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlSettings", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_settings.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlSettings", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_settings.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlSettings", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_settings.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlUserEditor", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_user_editor.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlUserEditor", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_user_editor.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlUserEditor", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_user_editor.html", null ],
      [ "Sokoban.WpfApp.Windows.GameWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_game_window.html", null ],
      [ "Sokoban.WpfApp.Windows.GameWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_game_window.html", null ],
      [ "Sokoban.WpfApp.Windows.GameWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_game_window.html", null ],
      [ "Sokoban.WpfApp.Windows.MainWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_main_window.html", null ],
      [ "Sokoban.WpfApp.Windows.MainWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_main_window.html", null ],
      [ "Sokoban.WpfApp.Windows.MainWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_main_window.html", null ]
    ] ],
    [ "Sokoban.Logic.ILevelLogic", "interface_sokoban_1_1_logic_1_1_i_level_logic.html", [
      [ "Sokoban.Logic.LevelLogic", "class_sokoban_1_1_logic_1_1_level_logic.html", null ]
    ] ],
    [ "IMultiValueConverter", null, [
      [ "Sokoban.WpfApp.Converters.SettingToViewboxRectConverter", "class_sokoban_1_1_wpf_app_1_1_converters_1_1_setting_to_viewbox_rect_converter.html", null ]
    ] ],
    [ "Sokoban.Logic.IMusicLogic", "interface_sokoban_1_1_logic_1_1_i_music_logic.html", [
      [ "Sokoban.Logic.MusicLogic", "class_sokoban_1_1_logic_1_1_music_logic.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Sokoban.Logic.ISettingLogic", "interface_sokoban_1_1_logic_1_1_i_setting_logic.html", [
      [ "Sokoban.Logic.SettingLogic", "class_sokoban_1_1_logic_1_1_setting_logic.html", null ]
    ] ],
    [ "Sokoban.Logic.IUserLogic", "interface_sokoban_1_1_logic_1_1_i_user_logic.html", [
      [ "Sokoban.Logic.UserLogic", "class_sokoban_1_1_logic_1_1_user_logic.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "Sokoban.WpfApp.Converters.ClearedToBrushConverter", "class_sokoban_1_1_wpf_app_1_1_converters_1_1_cleared_to_brush_converter.html", null ]
    ] ],
    [ "ObservableObject", null, [
      [ "Sokoban.Model.Level", "class_sokoban_1_1_model_1_1_level.html", null ],
      [ "Sokoban.Model.Music", "class_sokoban_1_1_model_1_1_music.html", null ],
      [ "Sokoban.Model.Setting", "class_sokoban_1_1_model_1_1_setting.html", null ],
      [ "Sokoban.Model.Tile", "class_sokoban_1_1_model_1_1_tile.html", null ],
      [ "Sokoban.Model.User", "class_sokoban_1_1_model_1_1_user.html", null ]
    ] ],
    [ "Sokoban.WpfApp.Properties.Resources", "class_sokoban_1_1_wpf_app_1_1_properties_1_1_resources.html", null ],
    [ "UserControl", null, [
      [ "Sokoban.WpfApp.UserControls.UserControlDashboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_dashboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLeaderboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_leaderboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLevels", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_levels.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlSettings", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_settings.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlUserEditor", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_user_editor.html", null ]
    ] ],
    [ "UserControl", null, [
      [ "Sokoban.WpfApp.UserControls.UserControlDashboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_dashboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlDashboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_dashboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlDashboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_dashboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLeaderboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_leaderboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLeaderboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_leaderboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLeaderboard", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_leaderboard.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLevels", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_levels.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLevels", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_levels.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlLevels", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_levels.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlSettings", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_settings.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlSettings", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_settings.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlSettings", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_settings.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlUserEditor", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_user_editor.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlUserEditor", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_user_editor.html", null ],
      [ "Sokoban.WpfApp.UserControls.UserControlUserEditor", "class_sokoban_1_1_wpf_app_1_1_user_controls_1_1_user_control_user_editor.html", null ]
    ] ],
    [ "ViewModelBase", null, [
      [ "Sokoban.ViewModel.LevelViewModel", "class_sokoban_1_1_view_model_1_1_level_view_model.html", null ],
      [ "Sokoban.ViewModel.MainViewModel", "class_sokoban_1_1_view_model_1_1_main_view_model.html", null ],
      [ "Sokoban.ViewModel.SettingViewModel", "class_sokoban_1_1_view_model_1_1_setting_view_model.html", null ],
      [ "Sokoban.ViewModel.UserViewModel", "class_sokoban_1_1_view_model_1_1_user_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "Sokoban.WpfApp.Windows.GameWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_game_window.html", null ],
      [ "Sokoban.WpfApp.Windows.GameWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_game_window.html", null ],
      [ "Sokoban.WpfApp.Windows.GameWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_game_window.html", null ],
      [ "Sokoban.WpfApp.Windows.MainWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_main_window.html", null ],
      [ "Sokoban.WpfApp.Windows.MainWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_main_window.html", null ],
      [ "Sokoban.WpfApp.Windows.MainWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "Sokoban.WpfApp.Windows.GameWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_game_window.html", null ],
      [ "Sokoban.WpfApp.Windows.MainWindow", "class_sokoban_1_1_wpf_app_1_1_windows_1_1_main_window.html", null ]
    ] ]
];