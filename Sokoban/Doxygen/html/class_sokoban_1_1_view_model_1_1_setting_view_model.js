var class_sokoban_1_1_view_model_1_1_setting_view_model =
[
    [ "SettingViewModel", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a8a268c122d18b8dcd6e4448d4dbd8ce8", null ],
    [ "SettingViewModel?", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a059007d87def2966db8c8a0b722bab9a", null ],
    [ "FloarGoalSettings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a88b81103757c68ff7479500cf6482c1d", null ],
    [ "FloarSettings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a0feafd238589100af9b3435792fa80f9", null ],
    [ "PackageGoalSettings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a2bc85d1ca5e3dac65d43b317c254a510", null ],
    [ "PackageSettings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a179c06e8972867df1f4ca45df20509a1", null ],
    [ "PlayerSettings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#ae6a8c7627c334826b2df98165533a0a1", null ],
    [ "SavedSettings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#abe4278520b309d66aec97141874a7854", null ],
    [ "SaveSettings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a3f87a0400162c7e190bbd566ee09ec29", null ],
    [ "SelectedFloarGoalSetting", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a6d7ebd6e47bf66cfcb67f13d4eaabec3", null ],
    [ "SelectedFloarSetting", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#aa152ae8f7cf11969da34dc121161faae", null ],
    [ "SelectedPackageGoalSetting", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#ac7515b7b1e75bcc8d47d2e3b49d37d5f", null ],
    [ "SelectedPackageSetting", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a46401d94d57b0b5b9f964eca3dad99da", null ],
    [ "SelectedPlayerSettings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#a52fd714819d05bf7e34afa96dbb150b0", null ],
    [ "SelectedWallSetting", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#ae5c62a924f21796f5ef8c229fbc739b2", null ],
    [ "Settings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#ad345b0043be92d021e05e56935292819", null ],
    [ "WallSettings", "class_sokoban_1_1_view_model_1_1_setting_view_model.html#abffb85d26755a73ba6c53d9de4d97029", null ]
];