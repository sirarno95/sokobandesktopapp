var interface_sokoban_1_1_logic_1_1_i_setting_logic =
[
    [ "LoadFloorGoalSetting", "interface_sokoban_1_1_logic_1_1_i_setting_logic.html#af550bb3588400075de13b3db7fd9886a", null ],
    [ "LoadFloorSetting", "interface_sokoban_1_1_logic_1_1_i_setting_logic.html#a12ea682b0c06eaef4e182d4fd7f55090", null ],
    [ "LoadPackatGoalSetting", "interface_sokoban_1_1_logic_1_1_i_setting_logic.html#ab6c7f93e852d3985d6310dd856ee2a4c", null ],
    [ "LoadPackatSetting", "interface_sokoban_1_1_logic_1_1_i_setting_logic.html#acb35e6241e088a395459f74f6d0a3419", null ],
    [ "LoadPlayerSetting", "interface_sokoban_1_1_logic_1_1_i_setting_logic.html#ae78b4d80272eb2a85d7ebb32fcb0aa9d", null ],
    [ "LoadSavedSetting", "interface_sokoban_1_1_logic_1_1_i_setting_logic.html#aecb783f683f977e6d6866859792b9a40", null ],
    [ "LoadWallSetting", "interface_sokoban_1_1_logic_1_1_i_setting_logic.html#a9e7c87ecfc44fa09f9cad94f6f293cc1", null ],
    [ "SaveSettings", "interface_sokoban_1_1_logic_1_1_i_setting_logic.html#a094d87765d35669c99cacd262a1febbb", null ]
];