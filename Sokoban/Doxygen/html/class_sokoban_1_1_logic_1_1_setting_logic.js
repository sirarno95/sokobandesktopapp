var class_sokoban_1_1_logic_1_1_setting_logic =
[
    [ "SettingLogic", "class_sokoban_1_1_logic_1_1_setting_logic.html#abf79534f669b1c6cfe3efbfe2df8fec9", null ],
    [ "LoadFloorGoalSetting", "class_sokoban_1_1_logic_1_1_setting_logic.html#a22b9044b5987020b57b7edb1545a59bc", null ],
    [ "LoadFloorSetting", "class_sokoban_1_1_logic_1_1_setting_logic.html#ad54254354015ba88ff461d8de0150f45", null ],
    [ "LoadPackatGoalSetting", "class_sokoban_1_1_logic_1_1_setting_logic.html#a7e4c67ea95bccc0c86547cbad2ad49b8", null ],
    [ "LoadPackatSetting", "class_sokoban_1_1_logic_1_1_setting_logic.html#a3b3686a071d19b56b32f7d4377738a45", null ],
    [ "LoadPlayerSetting", "class_sokoban_1_1_logic_1_1_setting_logic.html#a6a613ef5cffc03a8ae57a24b3b236004", null ],
    [ "LoadSavedSetting", "class_sokoban_1_1_logic_1_1_setting_logic.html#adf6a492d141771c5e45c8784f065b51a", null ],
    [ "LoadSetting", "class_sokoban_1_1_logic_1_1_setting_logic.html#a2502b468ce53a0c21db81688b8980b9e", null ],
    [ "LoadWallSetting", "class_sokoban_1_1_logic_1_1_setting_logic.html#a5bef9404a3b0da59d01f14df7d48e38b", null ],
    [ "SaveSettings", "class_sokoban_1_1_logic_1_1_setting_logic.html#a0d3f476f89aba730cb4581594926ccb2", null ]
];