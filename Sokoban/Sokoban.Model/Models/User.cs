﻿// <copyright file="User.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Model
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// asd.
    /// </summary>
    public class User : ObservableObject
    {
        private string nickName;

        /// <summary>
        /// Gets or sets nickName property.
        /// </summary>
        public string NickName
        {
            get => this.nickName;
            set => this.Set(ref this.nickName, value);
        }
    }
}
