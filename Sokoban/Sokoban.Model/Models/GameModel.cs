﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Model
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// SocobanModel.
    /// </summary>
    public class GameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="w">Game width parameter.</param>
        /// <param name="h">Game height parameter.</param>
        public GameModel(double w, double h)
        {
            this.WindowWidth = w;
            this.WindowHeight = h;
        }

        /// <summary>
        /// Gets or sets gameWidth.
        /// </summary>
        public double GameWidth { get; set; }

        /// <summary>
        /// Gets or sets gameHeight.
        /// </summary>
        public double GameHeight { get; set; }

        /// <summary>
        /// Gets or sets floors tiles coordinates.
        /// </summary>
        public bool[,] Floor { get; set; }

        /// <summary>
        /// Gets or sets walls tiles coordinates.
        /// </summary>
        public bool[,] Walls { get; set; }

        /// <summary>
        /// Gets or sets packages tiles coordinates.
        /// </summary>
        public bool[,] Packages { get; set; }

        /// <summary>
        /// Gets or sets packages tiles coordinates.
        /// </summary>
        public List<Point> PackagePoints { get; set; }

        /// <summary>
        /// Gets or sets packages tiles coordinates.
        /// </summary>
        public Point MovedPackagePoint { get; set; }

        /// <summary>
        /// Gets or sets packages on gole tiles coordinates.
        /// </summary>
        public bool[,] PackagesOnGoal { get; set; }

        /// <summary>
        /// Gets or sets player tile coordinates.
        /// </summary>
        public Point Player { get; set; }

        /// <summary>
        /// Gets or sets player on gole tile coordinates.
        /// </summary>
        public Point PlayerOnGoal { get; set; }

        /// <summary>
        /// Gets game width pixel size.
        /// </summary>
        public double WindowWidth { get; private set; }

        /// <summary>
        /// Gets game height pixel size.
        /// </summary>
        public double WindowHeight { get; private set; }

        /// <summary>
        /// Gets or sets one tile pixel size.
        /// </summary>
        public double TileSize { get; set; }

        /// <summary>
        /// Gets or sets settings property.
        /// </summary>
        public JObject Setting { get; set; }

        /// <summary>
        /// Gets or sets image.
        /// </summary>
        public BitmapImage TheImage { get; set; }

        /// <summary>
        /// Gets or sets background image.
        /// </summary>
        public BitmapImage BackgroundImage { get; set; }

        /// <summary>
        /// Gets or sets reset button image.
        /// </summary>
        public BitmapImage ResetButtonImage { get; set; }

        /// <summary>
        /// Gets or sets exit button image.
        /// </summary>
        public BitmapImage ExitButtonImage { get; set; }

        /// <summary>
        /// Gets or sets back button image.
        /// </summary>
        public BitmapImage MoveBackButtonImage { get; set; }

        /// <summary>
        /// Gets or sets current level id.
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Gets or sets current level id.
        /// </summary>
        public Stopwatch GameTime { get; set; }

        /// <summary>
        /// Gets or sets player moves.
        /// </summary>
        public int Moves { get; set; } = 0;

        /// <summary>
        /// Gets or sets package pushes.
        /// </summary>
        public int Pushes { get; set; } = 0;

        /// <summary>
        /// Gets or sets player moves.
        /// </summary>
        public int Helps { get; set; } = 0;

        /// <summary>
        /// Gets or sets current direction.
        /// </summary>
        public Direction CurrentDirection { get; set; }

        /// <summary>
        /// Gets or sets moving directions.
        /// </summary>
        public List<Direction> Directions { get; set; }

        /// <summary>
        /// Gets or sets package pushes.
        /// </summary>
        public List<bool> PackagePushes { get; set; }

        /// <summary>
        /// Gets or sets saved packages.
        /// </summary>
        public List<bool[,]> SavedPackages { get; set; }

        /// <summary>
        /// Gets or sets saved package points.
        /// </summary>
        public List<List<Point>> SavedPackagePoints { get; set; }
    }
}
