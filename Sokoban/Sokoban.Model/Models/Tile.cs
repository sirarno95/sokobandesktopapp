﻿// <copyright file="Tile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Model
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// TIle class.
    /// </summary>
    public class Tile : ObservableObject
    {
        private string type;
        private string tileId;
        private string label;
        private int tileX;
        private int tileY;

        /// <summary>
        /// Gets or sets type property.
        /// </summary>
        public string Type
        {
            get { return this.type; }
            set { this.Set(ref this.type, value); }
        }

        /// <summary>
        /// Gets or sets tiles id.
        /// </summary>
        public string TileId
        {
            get { return this.tileId; }
            set { this.Set(ref this.tileId, value); }
        }

        /// <summary>
        /// Gets or sets tile label.
        /// </summary>
        public string Label
        {
            get { return this.label; }
            set { this.Set(ref this.label, value); }
        }

        /// <summary>
        /// Gets or sets tileX property.
        /// </summary>
        public int TileX
        {
            get { return this.tileX; }
            set { this.Set(ref this.tileX, value); }
        }

        /// <summary>
        /// Gets or sets tileY property.
        /// </summary>
        public int TileY
        {
            get { return this.tileY; }
            set { this.Set(ref this.tileY, value); }
        }
    }
}
