﻿// <copyright file="Level.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Model
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Level.
    /// </summary>
    public class Level : ObservableObject
    {
        private int levelId;
        private int width;
        private int height;
        private bool isCleared;

        /// <summary>
        /// Gets or sets levelId property.
        /// </summary>
        public int LevelId
        {
            get => this.levelId;
            set => this.Set(ref this.levelId, value);
        }

        /// <summary>
        /// Gets or sets widht property.
        /// </summary>
        public int Widht
        {
            get => this.width;
            set => this.Set(ref this.width, value);
        }

        /// <summary>
        /// Gets or sets height property.
        /// </summary>
        public int Height
        {
            get => this.height;
            set => this.Set(ref this.height, value);
        }

        /// <summary>
        /// Gets or sets a value indicating whether isCleared property.
        /// </summary>
        public bool IsCleared
        {
            get => this.isCleared;
            set => this.Set(ref this.isCleared, value);
        }
    }
}
