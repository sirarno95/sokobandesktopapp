﻿// <copyright file="GameDirection.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Model
{
    /// <summary>
    /// Direction of moving.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Up.
        /// </summary>
        Up,

        /// <summary>
        /// Down.
        /// </summary>
        Down,

        /// <summary>
        /// Left.
        /// </summary>
        Left,

        /// <summary>
        /// Right
        /// </summary>
        Right,

        /// <summary>
        /// None.
        /// </summary>
        None,
    }
}
