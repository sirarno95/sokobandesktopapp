﻿// <copyright file="Music.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Model
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Model of Music.
    /// </summary>
    public class Music : ObservableObject
    {
        private bool isPlay;
        private string myImgame;

        /// <summary>
        /// Gets or sets a value indicating whether music is playing.
        /// </summary>
        public bool IsPlay
        {
            get { return this.isPlay; }
            set { this.Set(ref this.isPlay, value); }
        }

        /// <summary>
        /// Gets or sets music image name.
        /// </summary>
        public string MyImage
        {
            get { return this.myImgame; }
            set { this.Set(ref this.myImgame, value); }
        }
    }
}
