﻿// <copyright file="Setting.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.Model
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Setting model.
    /// </summary>
    public class Setting : ObservableObject
    {
        private string settingType;
        private string settingValue;

        /// <summary>
        /// Gets or sets type of setting.
        /// </summary>
        public string Type
        {
            get { return this.settingType; }
            set { this.Set(ref this.settingType, value); }
        }

        /// <summary>
        /// Gets or sets value of setting.
        /// </summary>
        public string Value
        {
            get { return this.settingValue; }
            set { this.Set(ref this.settingValue, value); }
        }
    }
}
