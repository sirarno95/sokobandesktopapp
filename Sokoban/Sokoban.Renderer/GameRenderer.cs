﻿// <copyright file="GameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
#pragma warning disable CS0618 // Type or member is obsolete

namespace Sokoban.Renderer
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;
    using Newtonsoft.Json.Linq;
    using Sokoban.Model;

    /// <summary>
    /// SokobanRenderer.
    /// </summary>
    public class GameRenderer
    {
        private readonly int maxsteps = 8;
        private readonly GameModel model;
        private readonly string itemsImgName = "items.png";
        private readonly string bgImgName = "sokoban_empty_bg.png";
        private readonly string resetImgName = "sokoban_empty_bg.png";
        private readonly string exitImgName = "menu_button.png";
        private readonly string moveBackImgName = "back_button.png";
        private readonly Dictionary<string, Brush> myBrushes = new Dictionary<string, Brush>();

        private int steps = 1;
        private Drawing background;
        private Drawing resetButton;
        private Drawing exitButton;
        private Drawing moveBackButton;
        private Drawing floors;
        private Drawing walls;
        private Drawing packages;
        private Drawing packagesOnGoal;
        private Drawing movedPackage;
        private Drawing player;
        private Drawing playerOnGoal;
        private Point playerPosition; // Tile position
        private Point movedPackagePosition; // Tile position

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="model">Instance of the <see cref="GameModel"/> class.</param>
        public GameRenderer(GameModel model)
        {
            this.model = model;
        }

        private Brush BackgroundBrush => this.GetBackgroundImageBrush();

        private Brush ResetButtonBrush => this.GetResetButtonImageBrush();

        private Brush MoveBackButtonBrush => this.GetMoveBackButtonImageBrush();

        private Brush ExitButtonBrush => this.GetExitButtonImageBrush();

        private Brush FloorBrush => this.GetViewboxBrush("floor_brush", true);

        private Brush PlayerOnGoalBrush => this.GetViewboxBrush("floor_goal_brush", false);

        private Brush WallBrush => this.GetViewboxBrush("wall_brush", true);

        private Brush PackageBrush => this.GetViewboxBrush("packat_brush", true);

        private Brush MovedPackageBrush => this.GetViewboxBrush("packat_brush", false);

        private Brush PackatOnGoalBrush => this.GetViewboxBrush("packat_goal_brush", true);

        private Brush PlayerUp1Brush => this.GetPlayerBrush(3, 4, false);

        private Brush PlayerUp2Brush => this.GetPlayerBrush(4, 4, false);

        private Brush PlayerUp3Brush => this.GetPlayerBrush(5, 4, false);

        private Brush PlayerDown1Brush => this.GetPlayerBrush(0, 4, false);

        private Brush PlayerDown2Brush => this.GetPlayerBrush(1, 4, false);

        private Brush PlayerDown3Brush => this.GetPlayerBrush(2, 4, false);

        private Brush PlayerRight1Brush => this.GetPlayerBrush(0, 6, false);

        private Brush PlayerRight2Brush => this.GetPlayerBrush(1, 6, false);

        private Brush PlayerRight3Brush => this.GetPlayerBrush(2, 6, false);

        private Brush PlayerLeft1Brush => this.GetPlayerBrush(3, 6, false);

        private Brush PlayerLeft2Brush => this.GetPlayerBrush(4, 6, false);

        private Brush PlayerLeft3Brush => this.GetPlayerBrush(5, 6, false);

        /// <summary>
        /// Reset game datas.
        /// </summary>
        public void Reset()
        {
            this.floors = null;
            this.walls = null;
            this.packages = null;
            this.packagesOnGoal = null;
            this.movedPackage = null;
            this.player = null;
            this.packagesOnGoal = null;
            this.movedPackagePosition = new Point(-1, -1);
            this.playerPosition = new Point(-1, -1);
            this.myBrushes.Clear();
        }

        /// <summary>
        /// Build a drowing group.
        /// </summary>
        /// <param name="isAllPackageIsCorrect">Package is goal or not.</param>
        /// <returns>Drowing group.</returns>
        public Drawing BuildDrawing(bool isAllPackageIsCorrect)
        {
            DrawingGroup dg = new DrawingGroup();

            dg.Children.Add(this.GetBackGround());
            dg.Children.Add(this.GetResetButton());
            dg.Children.Add(this.GetMoveBackButton());
            dg.Children.Add(this.GetExitButton());
            dg.Children.Add(this.GetLevelText());
            dg.Children.Add(this.GetTime());
            dg.Children.Add(this.GetMoves());
            dg.Children.Add(this.GetPushes());
            dg.Children.Add(this.GetHelps());
            dg.Children.Add(this.GetFloor());
            dg.Children.Add(this.GetWalls());
            dg.Children.Add(this.GetPackagesOnGoal());

            if (isAllPackageIsCorrect)
            {
                dg.Children.Add(this.GetPlayerOnGoal());
            }

            dg.Children.Add(this.GetPackages());

            if (this.model.MovedPackagePoint.X != -1)
            {
                dg.Children.Add(this.GetMovedPackage());
            }

            dg.Children.Add(this.GetPlayer());

            return dg;
        }

        private Brush GetPlayerBrush(int x, int y, bool isTiled)
        {
            bool isHighlighted = (bool)this.model.Setting["settings"].FirstOrDefault(jt => (string)jt["type"] == "is_highlighted")["value"];
            if (isHighlighted)
            {
                y++;
            }

            double myRectX = (x * (1.0 / 13.0)) + (1.0 / 13.0 * 0.01);
            double myRectY = (y * (1.0 / 8.0)) + (1.0 / 8.0 * 0.01);
            double myRectWidht = (1.0 / 13.0) - (1.0 / 13.0 * 0.02);
            double myRectHeight = (1.0 / 8.0) - (1.0 / 8.0 * 0.01);

            ImageBrush myImageBrush = new ImageBrush(this.model.TheImage)
            {
                Viewbox = new Rect(myRectX, myRectY, myRectWidht, myRectHeight),
                Stretch = Stretch.Fill,
            };

            if (isTiled)
            {
                myImageBrush.TileMode = TileMode.Tile;
                myImageBrush.Viewport = new Rect(0, 0, this.model.TileSize, this.model.TileSize);
                myImageBrush.ViewportUnits = BrushMappingMode.Absolute;
            }

            this.myBrushes[this.itemsImgName] = myImageBrush;

            return this.myBrushes[this.itemsImgName];
        }

        private Brush GetViewboxBrush(string brush_type, bool isTiled)
        {
            JToken burshValue = this.model.Setting["settings"].FirstOrDefault(jt => (string)jt["type"] == brush_type)["value"];
            int x = (int)this.model.Setting["tiles"].FirstOrDefault(jt => (string)jt["id"] == (string)burshValue)["tileX"];
            int y = (int)this.model.Setting["tiles"].FirstOrDefault(jt => (string)jt["id"] == (string)burshValue)["tileY"];

            ImageBrush myImageBrush = new ImageBrush(this.model.TheImage)
            {
                Viewbox = new Rect(x * (1.0 / 13.0), y * (1.0 / 8.0), 1.0 / 13.0, 1.0 / 8.0),
                Stretch = Stretch.Fill,
            };

            if (isTiled)
            {
                double rectX = (this.model.WindowWidth / 2) - (this.model.GameWidth / 2);
                double rectY = (this.model.WindowHeight / 2) - (this.model.GameHeight / 2);

                myImageBrush.TileMode = TileMode.Tile;

                // Viewport where to start renderering X Y coordinatas.
                myImageBrush.Viewport = new Rect(rectX, rectY, this.model.TileSize, this.model.TileSize);
                myImageBrush.ViewportUnits = BrushMappingMode.Absolute;
            }

            this.myBrushes[this.itemsImgName] = myImageBrush;

            return this.myBrushes[this.itemsImgName];
        }

        private Brush GetBackgroundImageBrush()
        {
            ImageBrush myImageBrush = new ImageBrush(this.model.BackgroundImage)
            {
                Stretch = Stretch.Fill,
            };

            this.myBrushes[this.bgImgName] = myImageBrush;

            return this.myBrushes[this.bgImgName];
        }

        private Brush GetResetButtonImageBrush()
        {
            ImageBrush myImageBrush = new ImageBrush(this.model.ResetButtonImage)
            {
                Stretch = Stretch.Fill,
            };

            this.myBrushes[this.resetImgName] = myImageBrush;

            return this.myBrushes[this.resetImgName];
        }

        private Brush GetMoveBackButtonImageBrush()
        {
            ImageBrush myImageBrush = new ImageBrush(this.model.MoveBackButtonImage)
            {
                Stretch = Stretch.Fill,
            };

            this.myBrushes[this.moveBackImgName] = myImageBrush;

            return this.myBrushes[this.moveBackImgName];
        }

        private Brush GetExitButtonImageBrush()
        {
            ImageBrush myImageBrush = new ImageBrush(this.model.ExitButtonImage)
            {
                Stretch = Stretch.Fill,
            };

            this.myBrushes[this.exitImgName] = myImageBrush;

            return this.myBrushes[this.exitImgName];
        }

        private Drawing GetBackGround()
        {
            if (this.background == null)
            {
                Geometry g = new RectangleGeometry(new Rect(0, 0, this.model.WindowWidth, this.model.WindowHeight));
                this.background = new GeometryDrawing(this.BackgroundBrush, null, g);
            }

            return this.background;
        }

        private Drawing GetResetButton()
        {
            double x = this.model.WindowWidth - 190;
            double y = (this.model.WindowHeight * 0.1 / 2) - 30;
            if (this.resetButton == null)
            {
                Geometry g = new RectangleGeometry(new Rect(x, y, 60, 60));
                this.resetButton = new GeometryDrawing(this.ResetButtonBrush, null, g);
            }

            return this.resetButton;
        }

        private Drawing GetExitButton()
        {
            double x = this.model.WindowWidth - 100;
            double y = (this.model.WindowHeight * 0.1 / 2) - 30;
            if (this.exitButton == null)
            {
                Geometry g = new RectangleGeometry(new Rect(x, y, 60, 60));
                this.exitButton = new GeometryDrawing(this.ExitButtonBrush, null, g);
            }

            return this.exitButton;
        }

        private Drawing GetMoveBackButton()
        {
            double x = this.model.WindowWidth - 280;
            double y = (this.model.WindowHeight * 0.1 / 2) - 30;
            if (this.moveBackButton == null)
            {
                Geometry g = new RectangleGeometry(new Rect(x, y, 60, 60));
                this.moveBackButton = new GeometryDrawing(this.MoveBackButtonBrush, null, g);
            }

            return this.moveBackButton;
        }

        private Drawing GetLevelText()
        {
            FormattedText formattedText = new FormattedText(
                $"LEVEL {this.model.LevelId}", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 32, Brushes.White);

            double x = (this.model.WindowHeight * 0.1 / 2) - 18;
            double y = (this.model.WindowHeight * 0.1 / 2) - 18;

            return new GeometryDrawing(Brushes.White, new Pen(Brushes.White, 1), formattedText.BuildGeometry(new Point(x, y)));
        }

        private Drawing GetTime()
        {
            FormattedText formattedText = new FormattedText(
                $"TIME: {this.model.GameTime.Elapsed:mm\\:ss}", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 32, Brushes.White);

            double x = (this.model.WindowHeight * 0.1 / 2) - 18;
            double y = (this.model.WindowHeight * 0.1 / 2) - 18;

            return new GeometryDrawing(Brushes.White, new Pen(Brushes.White, 1), formattedText.BuildGeometry(new Point(x + 200, y)));
        }

        private Drawing GetMoves()
        {
            FormattedText formattedText = new FormattedText(
                $"MOVES: {this.model.Moves}", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 32, Brushes.White);

            double x = (this.model.WindowHeight * 0.1 / 2) - 18;
            double y = (this.model.WindowHeight * 0.1 / 2) - 18;

            return new GeometryDrawing(Brushes.White, new Pen(Brushes.White, 1), formattedText.BuildGeometry(new Point(x + 450, y)));
        }

        private Drawing GetPushes()
        {
            FormattedText formattedText = new FormattedText(
                $"PUSHES: {this.model.Pushes}", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 32, Brushes.White);

            double x = (this.model.WindowHeight * 0.1 / 2) - 18;
            double y = (this.model.WindowHeight * 0.1 / 2) - 18;

            return new GeometryDrawing(Brushes.White, new Pen(Brushes.White, 1), formattedText.BuildGeometry(new Point(x + 700, y)));
        }

        private Drawing GetHelps()
        {
            FormattedText formattedText = new FormattedText(
               $"HELPS: {this.model.Helps}", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 32, Brushes.White);

            double x = (this.model.WindowHeight * 0.1 / 2) - 18;
            double y = (this.model.WindowHeight * 0.1 / 2) - 18;

            return new GeometryDrawing(Brushes.White, new Pen(Brushes.White, 1), formattedText.BuildGeometry(new Point(x + 950, y)));
        }

        private Drawing GetMovedPackage()
        {
            if (this.movedPackage == null || this.movedPackagePosition != this.model.MovedPackagePoint)
            {
                double rectX = (this.model.MovedPackagePoint.X * this.model.TileSize) + (this.model.WindowWidth / 2) - (this.model.GameWidth / 2);
                double rectY = (this.model.MovedPackagePoint.Y * this.model.TileSize) + (this.model.WindowHeight / 2) - (this.model.GameHeight / 2);
                double rectWidht = this.model.TileSize;
                double rectHeight = this.model.TileSize;

                Geometry g = new RectangleGeometry(new Rect(rectX, rectY, rectWidht, rectHeight));
                this.movedPackage = new GeometryDrawing(this.MovedPackageBrush, null, g);

                this.movedPackagePosition = this.model.MovedPackagePoint;
            }

            return this.movedPackage;
        }

        private Drawing GetPlayerOnGoal()
        {
            if (this.playerOnGoal == null)
            {
                double rectX = (this.model.PlayerOnGoal.X * this.model.TileSize) + (this.model.WindowWidth / 2) - (this.model.GameWidth / 2);
                double rectY = (this.model.PlayerOnGoal.Y * this.model.TileSize) + (this.model.WindowHeight / 2) - (this.model.GameHeight / 2);
                double rectWidht = this.model.TileSize;
                double rectHeight = this.model.TileSize;

                Geometry g = new RectangleGeometry(new Rect(rectX, rectY, rectWidht, rectHeight));
                this.playerOnGoal = new GeometryDrawing(this.PlayerOnGoalBrush, null, g);
            }

            return this.playerOnGoal;
        }

        private Drawing GetPlayer()
        {
            if (this.player == null || this.playerPosition != this.model.Player)
            {
                double rectX = (this.model.Player.X * this.model.TileSize) + (this.model.WindowWidth / 2) - (this.model.GameWidth / 2);
                double rectY = (this.model.Player.Y * this.model.TileSize) + (this.model.WindowHeight / 2) - (this.model.GameHeight / 2);
                double rectWidht = this.model.TileSize;
                double rectHeight = this.model.TileSize;

                Geometry g = new RectangleGeometry(new Rect(rectX, rectY, rectWidht, rectHeight));

                Vector sub = Point.Subtract(this.playerPosition, this.model.Player);

                if (Math.Abs(sub.X) == 1 || Math.Abs(sub.Y) == 1)
                {
                    if (sub.X < 0)
                    {
                        this.player = new GeometryDrawing(this.PlayerLeft1Brush, null, g);
                    }
                    else if (sub.X > 0)
                    {
                        this.player = new GeometryDrawing(this.PlayerRight1Brush, null, g);
                    }
                    else if (sub.Y < 0)
                    {
                        this.player = new GeometryDrawing(this.PlayerUp1Brush, null, g);
                    }
                    else
                    {
                        this.player = new GeometryDrawing(this.PlayerDown1Brush, null, g);
                    }
                }
                else
                {
                    if (sub.Y == 0.0)
                    {
                        if (sub.X > 0)
                        {
                            if (this.steps == this.maxsteps)
                            {
                                this.player = new GeometryDrawing(this.PlayerLeft1Brush, null, g);
                                this.steps = 1;
                            }
                            else
                            {
                                if (this.steps++ % 2 == 1)
                                {
                                    if ((this.steps - 1) % 4 == 1)
                                    {
                                        this.player = new GeometryDrawing(this.PlayerLeft2Brush, null, g);
                                    }
                                    else
                                    {
                                        this.player = new GeometryDrawing(this.PlayerLeft3Brush, null, g);
                                    }
                                }
                                else
                                {
                                    this.player = new GeometryDrawing(this.PlayerLeft1Brush, null, g);
                                }
                            }
                        }
                        else
                        {
                            if (this.steps == this.maxsteps)
                            {
                                this.player = new GeometryDrawing(this.PlayerRight1Brush, null, g);
                                this.steps = 1;
                            }
                            else
                            {
                                if (this.steps++ % 2 == 1)
                                {
                                    if ((this.steps - 1) % 4 == 1)
                                    {
                                        this.player = new GeometryDrawing(this.PlayerRight2Brush, null, g);
                                    }
                                    else
                                    {
                                        this.player = new GeometryDrawing(this.PlayerRight3Brush, null, g);
                                    }
                                }
                                else
                                {
                                    this.player = new GeometryDrawing(this.PlayerRight1Brush, null, g);
                                }
                            }
                        }
                    }
                    else if (sub.X == 0.0)
                    {
                        if (sub.Y > 0)
                        {
                            if (this.steps == this.maxsteps)
                            {
                                this.player = new GeometryDrawing(this.PlayerUp1Brush, null, g);
                                this.steps = 1;
                            }
                            else
                            {
                                if (this.steps++ % 2 == 1)
                                {
                                    if ((this.steps - 1) % 4 == 1)
                                    {
                                        this.player = new GeometryDrawing(this.PlayerUp2Brush, null, g);
                                    }
                                    else
                                    {
                                        this.player = new GeometryDrawing(this.PlayerUp3Brush, null, g);
                                    }
                                }
                                else
                                {
                                    this.player = new GeometryDrawing(this.PlayerUp1Brush, null, g);
                                }
                            }
                        }
                        else
                        {
                            if (this.steps == this.maxsteps)
                            {
                                this.player = new GeometryDrawing(this.PlayerDown1Brush, null, g);
                                this.steps = 1;
                            }
                            else
                            {
                                if (this.steps++ % 2 == 1)
                                {
                                    if ((this.steps - 1) % 4 == 1)
                                    {
                                        this.player = new GeometryDrawing(this.PlayerDown2Brush, null, g);
                                    }
                                    else
                                    {
                                        this.player = new GeometryDrawing(this.PlayerDown3Brush, null, g);
                                    }
                                }
                                else
                                {
                                    this.player = new GeometryDrawing(this.PlayerDown1Brush, null, g);
                                }
                            }
                        }
                    }
                    else
                    {
                        this.player = new GeometryDrawing(this.PlayerDown1Brush, null, g);
                    }
                }

                this.playerPosition = this.model.Player;
            }

            return this.player;
        }

        private Drawing GetPackagesOnGoal()
        {
            if (this.packagesOnGoal == null)
            {
                GeometryGroup g = new GeometryGroup();
                for (int x = 0; x < this.model.PackagesOnGoal.GetLength(0); x++)
                {
                    for (int y = 0; y < this.model.PackagesOnGoal.GetLength(1); y++)
                    {
                        if (this.model.PackagesOnGoal[x, y])
                        {
                            double rectX = (x * this.model.TileSize) + (this.model.WindowWidth / 2) - (this.model.GameWidth / 2);
                            double rectY = (y * this.model.TileSize) + (this.model.WindowHeight / 2) - (this.model.GameHeight / 2);
                            double rectWidht = this.model.TileSize;
                            double rectHeight = this.model.TileSize;

                            Geometry box = new RectangleGeometry(new Rect(rectX, rectY, rectWidht, rectHeight));
                            g.Children.Add(box);
                        }
                    }
                }

                this.packagesOnGoal = new GeometryDrawing(this.PackatOnGoalBrush, null, g);
            }

            return this.packagesOnGoal;
        }

        private Drawing GetPackages()
        {
            GeometryGroup g = new GeometryGroup();

            foreach (Point point in this.model.PackagePoints)
            {
                double rectX = (point.X * this.model.TileSize) + (this.model.WindowWidth / 2) - (this.model.GameWidth / 2);
                double rectY = (point.Y * this.model.TileSize) + (this.model.WindowHeight / 2) - (this.model.GameHeight / 2);
                double rectWidht = this.model.TileSize;
                double rectHeight = this.model.TileSize;

                Geometry package = new RectangleGeometry(new Rect(rectX, rectY, rectWidht, rectHeight));
                g.Children.Add(package);
            }

            this.packages = new GeometryDrawing(this.PackageBrush, null, g);

            return this.packages;
        }

        private Drawing GetWalls()
        {
            if (this.walls == null)
            {
                GeometryGroup g = new GeometryGroup();
                for (int x = 0; x < this.model.Walls.GetLength(0); x++)
                {
                    for (int y = 0; y < this.model.Walls.GetLength(1); y++)
                    {
                        if (this.model.Walls[x, y])
                        {
                            double rectX = (x * this.model.TileSize) + (this.model.WindowWidth / 2) - (this.model.GameWidth / 2);
                            double rectY = (y * this.model.TileSize) + (this.model.WindowHeight / 2) - (this.model.GameHeight / 2);
                            double rectWidht = this.model.TileSize;
                            double rectHeight = this.model.TileSize;
                            Geometry box = new RectangleGeometry(new Rect(rectX, rectY, rectWidht, rectHeight));
                            g.Children.Add(box);
                        }
                    }
                }

                this.walls = new GeometryDrawing(this.WallBrush, null, g);
            }

            return this.walls;
        }

        private Drawing GetFloor()
        {
            if (this.floors == null)
            {
                GeometryGroup g = new GeometryGroup();
                for (int x = 0; x < this.model.Floor.GetLength(0); x++)
                {
                    for (int y = 0; y < this.model.Floor.GetLength(1); y++)
                    {
                        if (this.model.Floor[x, y])
                        {
                            double rectX = (x * this.model.TileSize) + (this.model.WindowWidth / 2) - (this.model.GameWidth / 2);
                            double rectY = (y * this.model.TileSize) + (this.model.WindowHeight / 2) - (this.model.GameHeight / 2);
                            double rectWidht = this.model.TileSize;
                            double rectHeight = this.model.TileSize;
                            Geometry box = new RectangleGeometry(new Rect(rectX, rectY, rectWidht, rectHeight));
                            g.Children.Add(box);
                        }
                    }
                }

                this.floors = new GeometryDrawing(this.FloorBrush, null, g);
            }

            return this.floors;
        }
    }
}
#pragma warning restore CS0618 // Type or member is obsolete
