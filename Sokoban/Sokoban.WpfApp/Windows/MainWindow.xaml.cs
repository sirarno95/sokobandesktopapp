﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.WpfApp.Windows
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Sokoban.ViewModel;
    using Sokoban.WpfApp.UserControls;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.DataContext = new MainViewModel();
        }

        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            this.ButtonCloseMenu.Visibility = Visibility.Visible;
            this.ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            this.ButtonCloseMenu.Visibility = Visibility.Collapsed;
            this.ButtonOpenMenu.Visibility = Visibility.Visible;
        }

        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.GridMain.Children.Clear();
            UserControl usc;
            switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            {
                case "Dashboard":
                    usc = new UserControlDashboard();
                    this.GridMain.Children.Add(usc);
                    break;
                case "Levels":
                    usc = new UserControlLevels();
                    this.GridMain.Children.Add(usc);
                    break;
                case "Leaderboard":
                    usc = new UserControlLeaderboard();
                    this.GridMain.Children.Add(usc);
                    break;
                case "GameSettings":
                    usc = new UserControlSettings();
                    this.GridMain.Children.Add(usc);
                    break;
                default:
                    break;
            }
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch
            {
            }
        }

        private void ShutdownButton(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void AccountButton(object sender, RoutedEventArgs e)
        {
            this.GridMain.Children.Clear();
            UserControl usc = new UserControlUserEditor();
            this.GridMain.Children.Add(usc);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.GridMain.Children.Clear();
            UserControl usc = new UserControlDashboard();
            this.GridMain.Children.Add(usc);
        }
    }
}
