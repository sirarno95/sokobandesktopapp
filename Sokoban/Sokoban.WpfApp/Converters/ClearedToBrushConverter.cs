﻿// <copyright file="ClearedToBrushConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.WpfApp.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// ClearedToBrushConverter.
    /// </summary>
    public class ClearedToBrushConverter : IValueConverter
    {
        /// <summary>
        /// Convert VM => UI.
        /// </summary>
        /// <param name="value">VM value.</param>
        /// <param name="targetType">UI type.</param>
        /// <param name="parameter">XAML ConvertParameter.</param>
        /// <param name="culture">Thread culture.</param>
        /// <returns>UI Value.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isCleared = (bool)value;
            if (isCleared)
            {
                return new SolidColorBrush(Color.FromArgb(255, (byte)0, (byte)90, (byte)0));
            }
            else
            {
                return new SolidColorBrush(Color.FromArgb(255, (byte)229, (byte)90, (byte)0));
            }
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
