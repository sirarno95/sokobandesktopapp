﻿// <copyright file="SettingToViewboxRectConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.WpfApp.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using Sokoban.Model;

    /// <summary>
    /// SettingToViewboxRectConverter.
    /// </summary>
    public class SettingToViewboxRectConverter : IMultiValueConverter
    {
        /// <summary>
        /// Convert VM => UI.
        /// </summary>
        /// <param name="values">VM value.</param>
        /// <param name="targetType">UI type.</param>
        /// <param name="parameter">XAML ConvertParameter.</param>
        /// <param name="culture">Thread culture.</param>
        /// <returns>UI Value.</returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double x = double.Parse(values[0].ToString());
            double y = double.Parse(values[1].ToString());

            return new Rect(x * (1.0 / 13.0), y * (1.0 / 8.0), 1.0 / 13.0, 1.0 / 8.0);
        }

        /// <inheritdoc/>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
