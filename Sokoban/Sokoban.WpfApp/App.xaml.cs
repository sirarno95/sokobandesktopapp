﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban
{
    using System.Globalization;
    using System.Threading;
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using Sokoban.Logic;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            // SimpleIoc configuration.
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<IUserLogic, UserLogic>();
            SimpleIoc.Default.Register<ILevelLogic, LevelLogic>();
            SimpleIoc.Default.Register<ISettingLogic, SettingLogic>();
            SimpleIoc.Default.Register<IMusicLogic, MusicLogic>();
            SimpleIoc.Default.Register(() => Messenger.Default);

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
        }
    }
}
