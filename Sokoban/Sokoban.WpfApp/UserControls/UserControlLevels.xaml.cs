﻿// <copyright file="UserControlLevels.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.WpfApp.UserControls
{
    using System.Windows;
    using System.Windows.Controls;
    using Sokoban.ViewModel;
    using Sokoban.WpfApp.Windows;

    /// <summary>
    /// Interaction logic for UserControlLevels.xaml.
    /// </summary>
    public partial class UserControlLevels : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserControlLevels"/> class.
        /// </summary>
        public UserControlLevels()
        {
            this.InitializeComponent();
            this.DataContext = new LevelViewModel();
        }

        private void PlayButton(object sender, RoutedEventArgs e)
        {
            GameWindow game = new GameWindow();
            game.Show();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            (this.DataContext as LevelViewModel).OpenSokobnFunc = (level) =>
            {
                GameWindow game = new GameWindow
                {
                    DataContext = level,
                };
                game.Show();
                return true;
            };
        }
    }
}
