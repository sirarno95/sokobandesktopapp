﻿// <copyright file="UserControlSettings.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.WpfApp.UserControls
{
    using System.Windows;
    using System.Windows.Controls;
    using Sokoban.ViewModel;

    /// <summary>
    /// Interaction logic for UserControlSettings.xaml.
    /// </summary>
    public partial class UserControlSettings : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserControlSettings"/> class.
        /// </summary>
        public UserControlSettings()
        {
            this.InitializeComponent();
            this.DataContext = new SettingViewModel();
        }
    }
}
