﻿// <copyright file="UserControlLeaderboard.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.WpfApp.UserControls
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for UserControlLeaderboard.xaml.
    /// </summary>
    public partial class UserControlLeaderboard : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserControlLeaderboard"/> class.
        /// </summary>
        public UserControlLeaderboard()
        {
            this.InitializeComponent();
        }
    }
}
