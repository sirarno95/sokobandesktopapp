﻿// <copyright file="UserControlDashboard.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.WpfApp.UserControls
{
    using System.Windows;
    using System.Windows.Controls;
    using Sokoban.WpfApp.Windows;

    /// <summary>
    /// Interaction logic for UserControlDashboard.xaml.
    /// </summary>
    public partial class UserControlDashboard : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserControlDashboard"/> class.
        /// </summary>
        public UserControlDashboard()
        {
            this.InitializeComponent();
        }

        private void PlayButton(object sender, RoutedEventArgs e)
        {
            GameWindow game = new GameWindow();
            game.Show();
        }
    }
}
