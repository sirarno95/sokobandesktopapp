﻿// <copyright file="UserControlUserEditor.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.WpfApp.UserControls
{
    using System.Windows;
    using System.Windows.Controls;
    using Sokoban.ViewModel;

    /// <summary>
    /// Interaction logic for UserControlUserEditor.xaml.
    /// </summary>
    public partial class UserControlUserEditor : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserControlUserEditor"/> class.
        /// </summary>
        public UserControlUserEditor()
        {
            this.InitializeComponent();
            this.DataContext = new UserViewModel();
        }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            // DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            // DialogResult = false;
        }
    }
}
