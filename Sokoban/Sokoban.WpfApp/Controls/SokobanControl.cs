﻿// <copyright file="SokobanControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.WpfApp.Controls
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Sokoban.Logic;
    using Sokoban.Model;
    using Sokoban.Renderer;

    /// <summary>
    /// SokobanControl.
    /// </summary>
    public class SokobanControl : FrameworkElement
    {
        /// <summary>
        /// SelectedLevel dependencyProperty.
        /// </summary>
        public static readonly DependencyProperty SelectedLevelProperty =
            DependencyProperty.Register("SelectedLevel", typeof(int), typeof(SokobanControl), new PropertyMetadata(0));

        private readonly string mapsName = @"Sokoban.Logic.Resources.Maps.Simple.slc";

        private GameLogic logic;
        private GameRenderer renderer;
        private GameModel model;
        private DispatcherTimer tickTimer;
        private bool[] finished = new bool[] { false, false };

        /// <summary>
        /// Initializes a new instance of the <see cref="SokobanControl"/> class.
        /// </summary>
        public SokobanControl()
        {
            this.Loaded += this.SokobanControl_Loaded;
        }

        /// <summary>
        /// Gets or sets selectedLevel.
        /// </summary>
        public int SelectedLevel
        {
            get => (int)this.GetValue(SelectedLevelProperty);

            set => this.SetValue(SelectedLevelProperty, value);
        }

        /// <summary>
        /// Override the renderer to own.
        /// </summary>
        /// <param name="drawingContext">Current drowing contect.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                drawingContext.DrawDrawing(this.renderer.BuildDrawing(this.finished[0]));
            }
        }

        private void SokobanControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.model = new GameModel(this.ActualWidth, this.ActualHeight);
            this.logic = new GameLogic(this.model, this.mapsName, this.SelectedLevel);
            this.renderer = new GameRenderer(this.model);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                this.model.GameTime = new Stopwatch();
                this.tickTimer = new DispatcherTimer
                {
                    Interval = TimeSpan.FromMilliseconds(60),
                };
                this.tickTimer.Tick += this.TickTimer_Tick;
                this.tickTimer.Start();

                win.KeyDown += this.Win_KeyDown;
                this.MouseLeftButtonDown += this.SokobanControl_MouseLeftButtonDown;
            }

            this.logic.RefreshScreenEvent += (obj, args) => this.InvalidateVisual();
            this.InvalidateVisual();
            this.model.GameTime.Start();

            this.logic.MoveBackGameEvent += this.MoveBack;
            this.logic.RestartGameEvent += this.RestartGame;
            this.logic.ExitGameEvent += this.Close;
        }

        private void SokobanControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point coordinates = e.GetPosition(this);
            this.logic.MouseClick(coordinates);
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    this.logic.Move(0, -1);
                    break;
                case Key.Down:
                    this.logic.Move(0, 1);
                    break;
                case Key.Left:
                    this.logic.Move(-1, 0);
                    this.InvalidateVisual();
                    break;
                case Key.Right:
                    this.logic.Move(1, 0);
                    break;
                case Key.R:
                    this.RestartGame();
                    break;
                case Key.B:
                    this.MoveBack();
                    break;
                case Key.NumPad1:
                    Window.GetWindow(this).WindowState = WindowState.Minimized;
                    break;
                case Key.NumPad2:
                    Window.GetWindow(this).WindowState = WindowState.Maximized;
                    break;
                case Key.Escape:
                    this.Close();
                    break;
            }

            this.InvalidateVisual();
        }

        private void Close()
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to exit the game?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Information);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    this.model.GameTime.Stop();
                    Window win = Window.GetWindow(this);
                    win.Close();
                    break;
            }
        }

        private void RestartGame()
        {
            this.finished = new bool[2];
            this.renderer.Reset();
            this.logic.Restart();
        }

        private void MoveBack()
        {
            this.logic.MoveBack();
        }

        private void TickTimer_Tick(object sender, EventArgs e)
        {
            this.logic.TimerUpdate();
            this.finished = this.logic.FinishMove();
            if (this.finished[0] && this.finished[1])
            {
                this.model.GameTime.Stop();
                this.FinishGame();
            }
        }

        private void Close(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RestartGame(object sender, EventArgs e)
        {
            this.RestartGame();
        }

        private void MoveBack(object sender, EventArgs e)
        {
            this.logic.MoveBack();
        }

        private void FinishGame()
        {
            string message = $"FINISHED!\nMoves: {this.model.Moves}\nPushes: {this.model.Pushes}\nTime: {this.model.GameTime.Elapsed:mm\\:ss}";
            this.tickTimer.Stop();
            this.renderer.Reset();
            MessageBox.Show(message);
            this.finished = new bool[2];
            Window win = Window.GetWindow(this);
            win.Close();
        }
    }
}
