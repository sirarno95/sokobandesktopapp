﻿// <copyright file="UserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.ViewModel
{
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Sokoban.Logic;
    using Sokoban.Model;

    /// <summary>
    /// LevelViewModel.
    /// </summary>
    public class UserViewModel : ViewModelBase
    {
        private readonly IUserLogic userLogic;
        private User user;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserViewModel"/> class.
        /// </summary>
        public UserViewModel()
            : this(
                IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IUserLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserViewModel"/> class.
        /// </summary>
        /// <param name="userLogic">User logic.</param>
        public UserViewModel(IUserLogic userLogic)
        {
            this.userLogic = userLogic;

            User u = this.userLogic.LoadUser();
            this.User = u != null ? new User() { NickName = u.NickName } : new User();

            this.SaveUser = new RelayCommand(() => this.userLogic.SaveUser(this.User));
        }

        /// <summary>
        /// Gets or sets user property.
        /// </summary>
        public User User
        {
            get => this.user;
            set => this.Set(ref this.user, value);
        }

        /// <summary>
        /// Gets ICommand to save user.
        /// </summary>
        public ICommand SaveUser { get; private set; }
    }
}
