﻿// <copyright file="SettingViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.ViewModel
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Sokoban.Logic;
    using Sokoban.Model;

    /// <summary>
    /// SettingViewModel.
    /// </summary>
    public class SettingViewModel : ViewModelBase
    {
        private readonly ISettingLogic settingLogic;
        private Tile selectedFloarSetting;
        private Tile selectedFloarGoalSetting;
        private Tile selectedWallSetting;
        private Tile selectedPackageSetting;
        private Tile selectedPackageGoalSetting;
        private Tile selectedPlayerSetting;

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingViewModel"/> class.
        /// </summary>
        public SettingViewModel()
            : this(
                IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ISettingLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingViewModel"/> class.
        /// </summary>
        /// <param name="settingLogic">Setting logic.</param>
        public SettingViewModel(ISettingLogic settingLogic)
        {
            this.settingLogic = settingLogic;

            this.FloarSettings = new ObservableCollection<Tile>();
            this.FloarGoalSettings = new ObservableCollection<Tile>();
            this.WallSettings = new ObservableCollection<Tile>();
            this.PackageSettings = new ObservableCollection<Tile>();
            this.PackageGoalSettings = new ObservableCollection<Tile>();
            this.PlayerSettings = new ObservableCollection<Tile>();
            this.SavedSettings = new ObservableCollection<Setting>();

            this.FloarSettings = this.settingLogic.LoadFloorSetting();
            this.FloarGoalSettings = this.settingLogic.LoadFloorGoalSetting();
            this.WallSettings = this.settingLogic.LoadWallSetting();
            this.PackageSettings = this.settingLogic.LoadPackatSetting();
            this.PackageGoalSettings = this.settingLogic.LoadPackatGoalSetting();
            this.PlayerSettings = this.settingLogic.LoadPlayerSetting();
            this.SavedSettings = this.settingLogic.LoadSavedSetting();

            this.SaveSettings = new RelayCommand(() => this.settingLogic.SaveSettings(this.Settings));
        }

        /// <summary>
        /// Gets settings.
        /// </summary>
        public ObservableCollection<Tile> FloarSettings { get; private set; }

        /// <summary>
        /// Gets settings.
        /// </summary>
        public ObservableCollection<Tile> FloarGoalSettings { get; private set; }

        /// <summary>
        /// Gets settings.
        /// </summary>
        public ObservableCollection<Tile> WallSettings { get; private set; }

        /// <summary>
        /// Gets settings.
        /// </summary>
        public ObservableCollection<Tile> PackageSettings { get; private set; }

        /// <summary>
        /// Gets settings.
        /// </summary>
        public ObservableCollection<Tile> PackageGoalSettings { get; private set; }

        /// <summary>
        /// Gets settings.
        /// </summary>
        public ObservableCollection<Tile> PlayerSettings { get; private set; }

        /// <summary>
        /// Gets saved settings.
        /// </summary>
        public ObservableCollection<Setting> SavedSettings { get; private set; }

        /// <summary>
        /// Gets or sets selected floor setting property.
        /// </summary>
        public Tile SelectedFloarSetting
        {
            // get => this.selectedFloarSetting;
            get
            {
                if (this.selectedFloarSetting == null)
                {
                    string type = this.FloarSettings.FirstOrDefault().Type;

                    string value = (from x in this.SavedSettings
                                    where x.Type == type
                                    select x.Value).FirstOrDefault();

                    Tile tile = (from x in this.FloarSettings
                                 where x.TileId == value
                                 select x).FirstOrDefault();

                    return tile;
                }

                return this.selectedFloarSetting;
            }
            set => this.Set(ref this.selectedFloarSetting, value);
        }

        /// <summary>
        /// Gets or sets selected floor goal setting property.
        /// </summary>
        public Tile SelectedFloarGoalSetting
        {
            get
            {
                if (this.selectedFloarGoalSetting == null)
                {
                    string type = this.FloarGoalSettings.FirstOrDefault().Type;

                    string value = (from x in this.SavedSettings
                                    where x.Type == type
                                    select x.Value).FirstOrDefault();

                    Tile tile = (from x in this.FloarGoalSettings
                                 where x.TileId == value
                                 select x).FirstOrDefault();

                    return tile;
                }

                return this.selectedFloarGoalSetting;
            }

            set => this.Set(ref this.selectedFloarGoalSetting, value);
        }

        /// <summary>
        /// Gets or sets selected wall setting property.
        /// </summary>
        public Tile SelectedWallSetting
        {
            get
            {
                if (this.selectedWallSetting == null)
                {
                    string type = this.WallSettings.FirstOrDefault().Type;

                    string value = (from x in this.SavedSettings
                                    where x.Type == type
                                    select x.Value).FirstOrDefault();

                    Tile tile = (from x in this.WallSettings
                                 where x.TileId == value
                                 select x).FirstOrDefault();

                    return tile;
                }

                return this.selectedWallSetting;
            }

            set => this.Set(ref this.selectedWallSetting, value);
        }

        /// <summary>
        /// Gets or sets selected package setting property.
        /// </summary>
        public Tile SelectedPackageSetting
        {
            get
            {
                if (this.selectedPackageSetting == null)
                {
                    string type = this.PackageSettings.FirstOrDefault().Type;

                    string value = (from x in this.SavedSettings
                                    where x.Type == type
                                    select x.Value).FirstOrDefault();

                    Tile tile = (from x in this.PackageSettings
                                 where x.TileId == value
                                 select x).FirstOrDefault();

                    return tile;
                }

                return this.selectedPackageSetting;
            }
            set => this.Set(ref this.selectedPackageSetting, value);
        }

        /// <summary>
        /// Gets or sets selected package goal setting property.
        /// </summary>
        public Tile SelectedPackageGoalSetting
        {
            get
            {
                if (this.selectedPackageGoalSetting == null)
                {
                    string type = this.PackageGoalSettings.FirstOrDefault().Type;

                    string value = (from x in this.SavedSettings
                                    where x.Type == type
                                    select x.Value).FirstOrDefault();

                    Tile tile = (from x in this.PackageGoalSettings
                                 where x.TileId == value
                                 select x).FirstOrDefault();

                    return tile;
                }

                return this.selectedPackageGoalSetting;
            }
            set => this.Set(ref this.selectedPackageGoalSetting, value);
        }

        /// <summary>
        /// Gets or sets selected player setting property.
        /// </summary>
        public Tile SelectedPlayerSettings
        {
            get
            {
                bool value = bool.Parse((from x in this.SavedSettings
                                         where x.Type == "is_highlighted"
                                         select x.Value).FirstOrDefault());

                Tile tile = !value
                    ? (from x in this.PlayerSettings
                       where x.TileId == "player_normal_brush"
                       select x).FirstOrDefault()
                    : (from x in this.PlayerSettings
                       where x.TileId == "player_highlight_brush"
                       select x).FirstOrDefault();

                return tile;
            }
            set => this.Set(ref this.selectedPlayerSetting, value);
        }

        /// <summary>
        /// Gets ICommand to save settings.
        /// </summary>
        public ICommand SaveSettings { get; private set; }

        private Tile[] Settings
        {
            get
            {
                return new Tile[]
                {
                    this.SelectedFloarSetting,
                    this.SelectedFloarGoalSetting,
                    this.SelectedWallSetting,
                    this.SelectedPackageSetting,
                    this.SelectedPackageGoalSetting,
                    this.SelectedPlayerSettings,
                };
            }
        }
    }
}
