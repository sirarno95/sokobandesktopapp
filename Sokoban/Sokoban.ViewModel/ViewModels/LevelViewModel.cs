﻿// <copyright file="LevelViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Sokoban.Logic;
    using Sokoban.Model;

    /// <summary>
    /// LevelViewModel.
    /// </summary>
    public class LevelViewModel : ViewModelBase
    {
        private readonly ILevelLogic levelLogic;
        private Level selectedLevel;
        private ObservableCollection<Level> levels;

        /// <summary>
        /// Initializes a new instance of the <see cref="LevelViewModel"/> class.
        /// </summary>
        public LevelViewModel()
            : this(
                IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ILevelLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LevelViewModel"/> class.
        /// </summary>
        /// <param name="levelLogic">Level logic.</param>
        public LevelViewModel(ILevelLogic levelLogic)
        {
            this.levelLogic = levelLogic;
            this.Levels = new ObservableCollection<Level>();
            this.Levels = this.levelLogic.LoadLevels("Sokoban.Logic.Resources.Maps.Simple.slc");

            this.PlaySokoban = new RelayCommand(() => this.levelLogic.StartGame(this.SelectedLevel, this.OpenSokobnFunc));

            GameLogic.RefestDateEvent += this.SokobanLogic_RefestDate;
        }

        /// <summary>
        /// Gets or sets openSokobnFunc.
        /// </summary>
        public Func<Level, bool> OpenSokobnFunc { get; set; }

        /// <summary>
        /// Gets or sets levels property.
        /// </summary>
        public ObservableCollection<Level> Levels
        {
            get { return this.levels; }
            set { this.Set(ref this.levels, value); }
        }

        /// <summary>
        /// Gets or sets selectedLevel property.
        /// </summary>
        public Level SelectedLevel
        {
            get => this.selectedLevel;
            set => this.Set(ref this.selectedLevel, value);
        }

        /// <summary>
        /// Gets ICommand to Start the game.
        /// </summary>
        public ICommand PlaySokoban { get; private set; }

        private void SokobanLogic_RefestDate(object sender, int levelId)
        {
            this.Levels[levelId - 1].IsCleared = true;
            this.levelLogic.LevelIsClearded(levelId);
        }
    }
}
