﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Sokoban.ViewModel
{
    using System;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Sokoban.Logic;
    using Sokoban.Model;

    /// <summary>
    /// MainViewModel.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IUserLogic userLogic;
        private readonly IMusicLogic musicLogic;
        private User currentUser;
        private Music music;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(
                    IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IUserLogic>(),
                    IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMusicLogic>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="userLogic">UserLocic.</param>
        /// <param name="musicLogic">Musci logic.</param>
        public MainViewModel(IUserLogic userLogic, IMusicLogic musicLogic)
        {
            this.userLogic = userLogic;
            this.musicLogic = musicLogic;
            User user = this.userLogic.LoadUser();
            if (user != null)
            {
                this.CurrentUser = user;
            }

            this.music = this.musicLogic.LoadMusic();

            UserLogic.RefestDate += this.UserLogic_RefestDate;

            this.MusicControl = new RelayCommand(() => this.musicLogic.MusicControl());
        }

        /// <summary>
        /// Gets or sets current user property.
        /// </summary>
        public User CurrentUser
        {
            get => this.currentUser;
            set => this.Set(ref this.currentUser, value);
        }

        /// <summary>
        /// Gets or sets music obj.
        /// </summary>
        public Music Music
        {
            get { return this.music; }
            set { this.Set(ref this.music, value); }
        }

        /// <summary>
        /// Gets ICommand to control music.
        /// </summary>
        public ICommand MusicControl { get; private set; }

        private void UserLogic_RefestDate(object sender, EventArgs e)
        {
            User user = this.userLogic.LoadUser();
            if (user != null)
            {
                this.CurrentUser = user;
            }
        }
    }
}
